(function ($) {
    'use strict';

    angular.module('app').controller('AddController', AddController);

    AddController.$inject = ['$state', '$scope', 'Database', '$q', 'Constants', '$uibModal'];

    function AddController($state, $scope, Database, $q, Constants, $uibModal) {
        var vm = this;
        var deputy = {};
        var countRegistDeputy = 0;

        $scope.addClick = function (form) {
          console.log(form)
            if (form.$valid) {
                var data = {
                    "_id": uuid.v4(),
                    "name": $scope.name
                };
                Database.save('/deputy', data).then(function () {
                    $state.go('registration');
                })
            } else
                form.$setSubmitted();

        };

        $scope.closeClick = function () {
            $state.go('agenda');
        }

        $q.all([
            Database.get('/deputy'),
            Database.getRegistered()
        ]).then(function (res) {
            console.log(res)
        });


    }
})($);
