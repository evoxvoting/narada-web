(function (angular) {
    'use strict';

    angular.module('app').controller('RegistrationModalController', controller);

    controller.$inject = ['Constants', 'deputy', '$uibModalInstance'];

    function controller(Constants, deputy, $uibModalInstance) {
        var vm = this;
        vm.model = deputy;
        vm.close = close;
        genQrCode();

        function genQrCode() {
            var data = {
                "host": Constants.host,
                "deputyId": vm.model._id
            };
            vm.qrCode = JSON.stringify(data);
        }


        function close() {
            $uibModalInstance.close();
        }
    }
})(angular);