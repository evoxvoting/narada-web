(function ($) {
    'use strict';

    angular.module('app').controller('RegistrationController', RegistrationController);

    RegistrationController.$inject = ['$state', '$scope', '$timeout', 'Database', '$q', 'Constants', '$uibModal', 'OneSignalService'];

    function RegistrationController($state, $scope, $timeout, Database, $q, Constants, $uibModal, OneSignalService) {
        var vm = this;
        var deputy = {};
        var countRegistDeputy = 0;

        $scope.addClick = function () {
            $state.go('addDeputy');
        };

        $scope.clearAll = function () {
          $scope.deputies.forEach(function(deputy){
            if (deputy.status)
              vm.unreg(deputy);
            $timeout(function () {
              Database.delete('/deputy/' + deputy._id);
            }, 500);
          });
          $scope.deputies = [];
        }

        $scope.itemEnabled = function (item) {
          Database.save('/deputy/' + item._id, {_id: item._id, enabled: item.enabled})
        }

        function getList() {
          $q.all([
              Database.get('/deputy'),
              Database.getRegistered()
          ]).then(function (res) {
              for (var i = 0; i < res[0].length; i++) {
                  var isRegistered = res[1].find(function (item) {
                      return item._id == res[0][i]._id;
                  });
                  if (isRegistered) {
                    countRegistDeputy++;
                  }
                  res[0][i].status = isRegistered;
              }
              $scope.deputies = res[0];
              $scope.countDeputies = res[0].length;
              $scope.countRegistDeputies = countRegistDeputy;

              $scope.deputies.forEach(function (deputy) {
                if (deputy.deviceId) {
                  OneSignalService.getDevice(deputy.deviceId).then(function(response){
                    deputy.device = response.data;
                    if (!Object.keys(deputy.device.tags).length) {
                      setTag(deputy.deviceId, deputy.device.tags);
                      deputy.device.tags = { tag1: 'Група 1', tag2: 'Група 2', tag3: 'Група 3' }
                    }
                  });
                }
              });
          });
        }

        getList();

        function setTag(deviceId, tags) {
          tags = { tag1: 'Група 1', tag2: 'Група 2', tag3: 'Група 3' };
          OneSignalService.updateDevice(deviceId, {tags: tags});
        }

        function delTag(deviceId) {
          var tags = { tag1: '', tag2: '', tag3: '' };
          OneSignalService.updateDevice(deviceId, {tags: tags});
        }

        vm.unreg = function (item) {
          return Database.unregDeputy(item._id).then(function () {
            delTag(item.deviceId);
            item.device = {};
            return Database.save('/deputy/' + item._id, {_id: item._id, deviceId: false}).then(function () {
              getList();
            });
          });
        }

        vm.tag = function (item, tags, tag) {
          var temp = angular.copy(tags);
          temp[tag] = '';
          delete tags[tag];
          OneSignalService.updateDevice(item.deviceId, {tags: temp});
        }

        vm.open = function (deputy) {
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'templates/register-modal.html',
                controller: 'RegistrationModalController',
                controllerAs: '$ctrl',
                size: 'm',
                resolve: {
                    deputy: function () {
                        return deputy;
                    }
                }
            });
            modalInstance.result
                .finally(function () {
                    getList();
                });
        };
    }
})($);
