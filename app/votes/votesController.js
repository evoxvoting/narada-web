/**
 * Created by Alexandra on 8/26/2016.
 */
(function(){
    'use strict';

    angular.module('app').controller('VotesController', VotesController);

    VotesController.$inject = ['$state','$scope','$stateParams','Database','Constants','$q'];

    function VotesController($state,$scope,$stateParams,Database,Constants,$q){
        $scope.type_answer = Constants.type_answer;
        getDeputies();

        function getDeputies(){
            var votes = $stateParams.issue.vote;
            $scope.title = $stateParams.issue.title;
            var deputy = [];
            $q.all([
                Database.get('/deputy'),
                Database.getRegistered()
            ]).then(function(res){
                var listAllDeputies = res[0];
                for(var i = 0; i < listAllDeputies.length;i++){
                    var vote = votes.accept.find(function (item) {
                        return item.deputy_id == listAllDeputies[i]._id;
                    });
                    if(!vote){
                        vote = votes.reject.find(function (item) {
                            return item.deputy_id == listAllDeputies[i]._id;
                        });
                        if(!vote){
                            vote = votes.abstained.find(function (item) {
                                return item.deputy_id == listAllDeputies[i]._id;
                            });
                            if(!vote) {
                                var isRegistered = res[1].find(function (item) {
                                    return item._id == listAllDeputies[i]._id;
                                });
                                if(isRegistered){
                                    vote = Constants.type_answer.no_vote;
                                } else{
                                    vote = Constants.type_answer.missing;
                                }
                                listAllDeputies[i].vote = vote;
                            }
                        }
                    }
                    listAllDeputies[i].vote = !listAllDeputies[i].vote? vote.vote : listAllDeputies[i].vote;
                }
                $scope.deputies = listAllDeputies;
            })

        }

        $scope.backClick = function() {
            $state.go('agenda');
        }
    }
})();