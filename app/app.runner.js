(function ($) {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngResource',
        'ngCookies',
        'app.config',
        'app.routes',
        'ja.qr',
        'ui.bootstrap'
    ]).run(runner);

    runner.$inject = ['$http', '$cookies', 'UrlPack'];

    function runner($http, $cookies, UrlPack) {
        //var authorization = 'Token ' + user.auth_token; //'Basic dGhvcjp0aG9y';
        var cfg = {
            //'Authorization': authorization,
            'Content-Type': 'application/json; charset=utf-8'
        };
        //var csrftoken = $cookies.get('csrftoken');
        //if (csrftoken) {
        //    cfg['X-CSRFToken'] = csrftoken;
        //}

        $http.defaults.headers.get = cfg;
        $http.defaults.headers.post = cfg;
        $http.defaults.headers.put = cfg;
        $http.defaults.headers.patch = cfg;
        $http.defaults.headers.options = cfg;
    }
})(jQuery);