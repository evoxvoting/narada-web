(function () {
    'use strict';

    angular.module('app.config', []).config(configure);

    configure.$inject = ['$provide', '$urlMatcherFactoryProvider'];

    function configure($provide, $urlMatcherFactory) {

        $urlMatcherFactory.type('boolean',
            {
                name: 'boolean',
                decode: function (val) {
                    return val == true ? true : val == "true"
                },
                encode: function (val) {
                    return val ? 1 : 0;
                },
                equals: function (a, b) {
                    return this.is(a) && a === b;
                },
                is: function (val) {
                    return [true, false, 0, 1].indexOf(val) >= 0
                },
                pattern: /bool|true|0|1/
            }
        );

        $provide.constant('UrlPack', {
            views: {
                agenda: VIEW_ROOT_URL + 'app/agenda/agenda.html',
                add: VIEW_ROOT_URL + 'app/registration/add.html',
                registration: VIEW_ROOT_URL + 'app/registration/registration.html',
                newQuestion: VIEW_ROOT_URL + 'app/newQuestion/newQuestion.html',
                votes: VIEW_ROOT_URL + 'app/votes/votes.html'
            }
        });

        $provide.constant('Constants',{
            host: 'http://evox.vareger.com:8090',
            type_answer: {
                yes: 'yes',
                no: 'no',
                abstained: 'abstained',
                no_vote: 'no_vote',
                missing: 'missing'
            }
        });

        $provide.constant('User',{
            password: 'opun',
            login: 'opun'
        });
    }
})();
