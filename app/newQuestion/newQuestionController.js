/**
 * Created by Alexandra on 8/26/2016.
 */
(function () {
    'use strict';

    angular.module('app').controller('NewQuestionController', NewQuestionController);

    NewQuestionController.$inject = ['$state', '$scope', 'Database'];

    function NewQuestionController($state, $scope, Database) {

        $scope.addClick = function (form) {
          console.log(form)
            if (form.$valid) {
                var data = {
                    "_id": uuid.v4(),
                    "title": $scope.title,
                    "description": $scope.description,
                    "group": $scope.group
                };
                Database.save('/issue', data).then(function () {
                    $state.go('agenda');
                })
            } else
                form.$setSubmitted();

        };

        $scope.closeClick = function () {
            $state.go('agenda');
        }
    }
})();
