/**
 * Created by Alexandra on 8/26/2016.
 */
(function ($) {
    'use strict';

    angular.module('app').controller('AgendaController', AgendaController);

    AgendaController.$inject = ['$state', '$scope', 'Database', '$q', '$timeout', 'Constants','AgendaFactory'];

    function AgendaController($state, $scope, Database, $q, $timeout, Constants,AgendaFactory) {

        var agendaFactory = AgendaFactory.create();
        var cacheIssues = agendaFactory.issues();

        if(cacheIssues){
            getAllVotes(cacheIssues).then(getAllStates)
        } else{
            Database.get('/issue').then(function(res){
                agendaFactory.issues(res);
                return getAllVotes(res);
            }).then(getAllStates)
        }

        function getAllVotes(res){
            var issues = [];
            var def = $q.defer();
            for (var i = 0; i < res.length; i++) {
                var isLast = i >= res.length - 1;
                getResultVoiting(res[i], isLast).then(function (result) {
                    issues.push(result.issue);
                    if (result.isLast) {
                        def.resolve(issues);
                    }
                }, def.reject);
            }
            return def.promise;
        }

        function getAllStates(data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].vote) {
                        if (data[i].vote.accept.length > (data[i].vote.reject.length + data[i].vote.abstained.length)) {
                            data[i].vote.state = 'accept';
                        } else {
                            data[i].vote.state = 'reject';
                        }
                    }
                }
                $scope.issues = data;
        }


        function getResultVoiting(issue, isLast) {
            var id = issue._id;
            var res = {};
            res.issue = issue;
            res.isLast = isLast;

            var vote = agendaFactory.vote(id);

            // if(vote){
            //     return sortVotes(res,vote);
            // } else {
                return Database.get('/voting/' + id).then(function (data) {
                    agendaFactory.vote(id,data);
                    return sortVotes(res,data);
                })
            //}
        }

        function sortVotes(res,data){
            var def = $q.defer();
            if (data.votes.length) {
              res.issue.vote = {
                  'accept': [],
                  'reject': [],
                  'abstained': []
              };
            }

            data.votes.forEach(function (vote) {
              switch (vote.vote) {
                  case Constants.type_answer.yes:
                      res.issue.vote.accept.push(vote);
                      break;
                  case Constants.type_answer.no:
                      res.issue.vote.reject.push(vote);
                      break;
                  case Constants.type_answer.abstained:
                      res.issue.vote.abstained.push(vote);
                      break;
              }
            });
            def.resolve(res);
            return def.promise;
        }

        $scope.addClick = function () {
            agendaFactory.destroy();
            $state.go('newQuestion');
        };

        $scope.clearAll = function () {
          $scope.issues.forEach(function(issue){
            Database.delete('/issue/' + issue._id)
          });
          $scope.issues = [];
        }

        $scope.startVoteClick = function (item) {
            $scope.model = item;
            $scope.modal_title2 = 'Питання: ' + item.title;
            Database.post('/voting/' + item._id).then(function () {
                startCounter(item)
            });
        };

        var mytimeout;

        $scope.closeClick = function() {
            $timeout.cancel(mytimeout);
            Database.cancelVoting($scope.model._id);
        };

        function startCounter(item) {
            $scope.counter = 20;
            $scope.onTimeout = function () {
                $scope.counter--;
                if ($scope.counter > 0) {
                    mytimeout = $timeout($scope.onTimeout, 1000);
                } else {
                    $('#counter-modal').modal('hide');
                    getResultVoiting(item).then(function (res) {
                        var issue = $scope.issues.find(function (item) {
                            return item._id == res.issue._id;
                        });
                        if (issue.vote) {
                            if (issue.vote.accept.length > (issue.vote.reject.length + issue.vote.abstained.length)) {
                                issue.vote.state = 'accept';
                            } else {
                                issue.vote.state = 'reject';
                            }
                        }
                    });
                }
            };
            mytimeout = $timeout($scope.onTimeout, 1000);
        }

        $scope.votesClick = function (issue) {
            $state.go('votes', {issue: issue});
        };
    }
})($);
