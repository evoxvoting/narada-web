(function(){
    'use strict';

    angular.module('app').controller('IndexController', IndexController);

    IndexController.$inject = ['$state'];

    function IndexController($state){

        $state.go('agenda');

    }
})();
