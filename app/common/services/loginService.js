(function (angular) {
    'use strict';

    angular.module('app').factory('Login', loginService);

    loginService.$inject = ['User'];

    function loginService(User) {

        function Login(){
            this.isLogin = false;
        }

        Login.sign = function(login,password){
            if(login == User.login && password == User.password) {
                window.localStorage.setItem('login', login);
                window.localStorage.setItem('password', password);
                this.isLogin = true;
            }
        }

        Login.signOut = function(){
            window.localStorage.removeItem('login');
            window.localStorage.removeItem('password');
            this.isLogin = false;
        }


        return {
            Login: Login,
            sign: Login.sign,
            signOut: Login.signOut
        }
    }
})(angular);