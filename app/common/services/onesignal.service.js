(function (angular) {
    'use strict';

    angular.module('app').service('OneSignalService', OneSignalService);

    OneSignalService.$inject = ['$http'];

    function OneSignalService($http) {

      var url = 'https://onesignal.com/api/v1/players/';
      var app_id = '30c7a2bb-74fc-410d-b622-ac76dbffa0e8';

      this.getDevice = function (id) {
        return $http.get(url + id + '?app_id=' + app_id);
      }

      this.updateDevice = function (id, data) {
        data.app_id = app_id;
        return $http.put(url + id, data);
      }
    }
})(angular);
