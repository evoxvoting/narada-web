(function (angular) {
    'use strict';

    angular.module('app').factory('AgendaFactory', factory);

    function factory($cacheFactory) {

        var current;
        var cache_key = 'issuesFactory';

        function AgendaFactory() {
            this.cache = $cacheFactory(cache_key);
        }

        var prototype = AgendaFactory.prototype;

        /**
         * Get/Set issues
         * @param issues
         * @returns {*}
         */
        prototype.issues = function (issues) {
            if (arguments.length > 0) {
                this.cache.put('issues', issues);
                return;
            }
            return this.cache.get('issues');
        };

        /**
         * Get/Set vote
         * @param issues
         * @returns {*}
         */
        prototype.vote = function (id,vote) {
            if (vote && vote != null) {
                this.cache.put('issueVotes.' + id, vote);
                return;
            }
            return this.cache.get('issueVotes.' + id);
        };

        prototype.destroy = function () {
            this.cache.destroy();
            current = null;
        };

        return {
            create: function () {
                if (!current) {
                    current = new AgendaFactory();
                }
                return current;
            }
        };
    }
})(angular);