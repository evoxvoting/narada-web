(function (angular) {
    'use strict';

    angular.module('app').factory('Database', databaseService);

    databaseService.$inject = ['$q','$http','Constants'];

    function databaseService($q, $http,Constants) {

        function get(db) {
             return $http.get(Constants.host + db)
                 .then(function(response) {
                     return response.data;
                 });
        }

        function save(db,data) {
            return $http.post(Constants.host + db,data)
                .success(function(response){
                    return true;
                }).catch(function(e){
                    console.log(e)
                    return false;
                })
        }

        function post(db) {
            return $http.post(Constants.host + db)
                .success(function(response){
                    return true;
                }).catch(function(e){
                    console.log(e)
                    return false;
                })
        }

        function remove(db) {
          return $http.delete(Constants.host + db)
              .success(function(response){
                  return true;
              }).catch(function(e){
                  console.log(e)
                  return false;
              })
        }

        function getRegistered() {
            return $http.get(Constants.host + '/deputy/registered')
                .then(function(response) {
                    return response.data;
                });
        }

        function unregDeputy(deputy_id) {
            return $http.post(Constants.host + '/deputy/' + deputy_id + '/unregister')
                .then(function(response) {
                    return response.data;
                });
        }

        function cancelVoting(issueId) {
            return $http.delete(Constants.host + '/voting/' + issueId)
                .then(function(response) {
                    return response.data;
                });
        }

        return {
            get: get,
            save: save,
            post: post,
            delete: remove,
            getRegistered: getRegistered,
            unregDeputy: unregDeputy,
            cancelVoting: cancelVoting
        }
    }
})(angular);
