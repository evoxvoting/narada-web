/**
 * Created by Ilya on 31.07.2015.
 */
(function(){
    'use strict';

    angular.module('app.routes', ['app.config']).config(configure);

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'UrlPack'];

    function configure($stateProvider, $urlRouterProvider, UrlPack) {
        $stateProvider
            .state('index', {
                url: "/index",
                controller: 'IndexController'
            })
            .state('agenda', {
                url: "/agenda",
                cache: true,
                templateUrl: UrlPack.views.agenda,
                controller: 'AgendaController'
            })
            .state('registration', {
                url: "/registration",
                cache: true,
                templateUrl: UrlPack.views.registration,
                controller: 'RegistrationController',
                controllerAs: '$ctrl'
            })
            .state('addDeputy', {
                url: "/add",
                templateUrl: UrlPack.views.add,
                controller: 'AddController',
                controllerAs: '$ctrl'
            })
            .state('newQuestion', {
                url: "/newQuestion",
                cache: true,
                templateUrl: UrlPack.views.newQuestion,
                controller: 'NewQuestionController'
            })
            .state('votes', {
                url: "/votes",
                params:{
                    issue: {}
                },
                cache: true,
                templateUrl: UrlPack.views.votes,
                controller: 'VotesController'
            })
        $urlRouterProvider.otherwise('index');
    }
})();
