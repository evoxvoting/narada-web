(function () {
    'use strict';

    angular.module('app').controller('AppController', AppController);

    AppController.$inject = ['$scope','Login'];

    function AppController($scope,Login) {
        $scope.model = {};

        var login = window.localStorage.getItem('login');
        if(login){
            Login.isLogin = true;
        }
        $scope.isLogin = Login.isLogin;

        $scope.submit = function(form,login,password){
            if(form.$valid){
                Login.sign($scope.model.login,$scope.model.password);
                $scope.isLogin = true;
            }
        }

        $scope.signOut = function(){
            Login.signOut();
            $scope.isLogin = false;
        }
    }
})();
